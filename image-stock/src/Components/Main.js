import React from 'react'
import styled from 'styled-components';
import { BsFillArrowDownSquareFill } from "react-icons/bs";
import {Context} from "../Context/Context";

export default function Main() {

    const { listImage, isloading, setShow, isshow, setSingleImg,title} =
      React.useContext(Context);
    const handeldatafetch = (images) => {
      setShow(!isshow);
      setSingleImg(images);
    };
    
    return (
      <>
        {isloading ? (
          <Header>Loading....</Header>
        ) : (
          <>
            <Header>{title}</Header>
            <Container>
              {listImage.map((image) => {
                return (
                  <Image>
                    <article>
                      <img
                        src={image.download_url}
                        alt="images"
                        onClick={() => handeldatafetch(image.id)}
                      />
                      <ImageInfo>
                        <div>
                          <h4 className="title">{image.author}</h4>
                        </div>
                        <BsFillArrowDownSquareFill
                          color="white"
                          fontSize="1.5em"
                        />
                      </ImageInfo>
                    </article>
                  </Image>
                );
              })}
            </Container>
          </>
        )}
      </>
    );
}


const Header = styled.h1`
text-align:center;
`

const Container = styled.div`
  /* max-width: 1200px; */
  padding: 0 25px;
  column-count: 3;
  /* margin: 0 auto; */
  margin-top:10px;
 
  @media (max-width: 768px) {
    column-count: 2;
  }
  @media (max-width: 500px) {
    column-count: 1;
  }
  
`;

const Image = styled.div`
  position: relative;
  margin-bottom: 10px;
  overflow: hidden;
  & img {
    width: 100%;
    height: 100%;
    display: block;
    object-fit: cover;
  }
`;

const ImageInfo = styled.div`
  position: absolute;
  display: flex;
  justify-content: space-between;
  bottom: 0;
  left: 0;
  width: 100%;
  padding: 8px 10px;
  background: rgba(0, 0, 0, 0.6);
  transform: translateY(100%);
  transition: all 0.3s linear;

  h4 {
    color: white;
  }
  ${Image}:hover & {
    transform: translateY(0);
  }
`;
