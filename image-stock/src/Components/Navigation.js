import React from 'react';
import styled from "styled-components";
import { Context } from "../Context/Context";

export default function Navigation() {


    

    const { setQuery, setTitle } = React.useContext(Context);
   

    const navList = [
      {id:1,
       name: "Editorials"},
     {id:2,
       name: "3DRenders"},
      {id:3,
       name: "architecture - interior"},
      {id:4,
       name: "current - event"},
      {id:6,
       name: "experimental"},
      {id:7,
       name: "fashion"},
      {id:8,
       name:"film"},
      {id:9,
       name: "food and Drink"},
      {id:10,
       name: "Health & wellness"},
      {id:11,
       name: "Nature"},
      {id:12,
      name: "People"}
    ];
    return <Section>
        <ul>
{navList.map((nav)=>{
  const handleClick =()=>{
setQuery(nav.id);
setTitle(nav.name);
  }

    return (
      <li>
        <a  key={nav.id}  onClick={handleClick}>{nav.name}</a>
      </li>
    );
})}
        </ul>
    </Section>;
}


const Section = styled.nav`
  max-width: 1200px;
  margin: 0 auto;
  margin-top: 10px;
  margin-bottom: 10px;
 

  ul {
    display: flex;
    justify-content: space-between;
    flex-wrap: nowrap;
    overflow: auto;
    white-space: nowrap;
    &::-webkit-scrollbar {
      width: 0;
      height: 0;
    }

    li {
      padding: 10px;
      list-style-type: none;
      a {
        text-decoration: none;
        color: #969693;
        &:hover {
          color: black;
        }
      }
    }
  }
`;