import React from "react";
import styled from "styled-components";
import { BsFillArrowDownSquareFill } from "react-icons/bs";
import { Context } from "../Context/Context";
import Masonry from "react-masonry-css";


export default function DMain() {
  const { listImage, isloading, setShow, isshow, setSingleImg } =
    React.useContext(Context);
  const handeldatafetch = (images) => {
    setShow(!isshow);
    setSingleImg(images);
  };

  return (
    <>
      {isloading ? (
        <Header>Loading....</Header>
      ) : (
        <Masonry
          breakpointCols={3}
          className="my-masonry-grid"
          columnClassName="my-masonry-grid_column"
        >
          
          {listImage.map((image) => {
            return (
              <Movie>
                <article>
                  <img
                    src={image.download_url}
                    alt="images"
                    onClick={() => handeldatafetch(image.id)}
                  />
                </article>
              </Movie>
            );
          })}
        </Masonry>
      )}
    </>
  );
}

const Header = styled.h1`
  text-align: center;
`;

const Container = styled.div`
max-width:1200px;
margin:0 auto;
display: grid;
gap:1em;
grid-template-columns: repeat(3,1fr);
grid-template-rows: masonry;
align-tracks :stretch ;
 
`;

const Movie = styled.div`
 
  width: 100%;

  overflow: hidden;
  & img {
    width: 100%;
    height: 100%;
    display: block;
    object-fit:contain;
  }
`;

const MovieInfo = styled.div`
  position: absolute;
  display: flex;
  justify-content: space-between;
  bottom: 0;
  left: 0;
  width: 100%;
  padding: 8px 10px;
  background: rgba(0, 0, 0, 0.6);
  transform: translateY(100%);
  transition: all 0.3s linear;

  h4 {
    color: white;
  }
  ${Movie}:hover & {
    transform: translateY(0);
  }
`;
