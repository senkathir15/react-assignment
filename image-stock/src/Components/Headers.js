import React from 'react'
import { Context } from '../Context/Context';
export default function Headers() {
   const { query } =
     React.useContext(Context);
    return (
      <>
      {query === 1 && <section className="section-one">
        <main className="section-overlay">
          <header>
            <h1>Unsplash</h1>
          </header>
          <input type="search" placeholder="Search" className="search"></input>
          <p>
            Lorem ipsum dolor sit amet consectetur adipisicing elit. Ipsum, qui
            non? amet consectetur adipisicing
          </p>
          {/* <button>Read More</button> */}
        </main>
      </section>}
      </>
    );
}
