import React from 'react';
import styled from 'styled-components';
import { Context } from "../Context/Context";
import {
  BsXLg,
  BsFillSuitHeartFill,
  BsChevronLeft,
  BsChevronRight,
} from "react-icons/bs";



export default function Modal() {
  const { setShow, isshow, singledata, setSingleImg } =
    React.useContext(Context);

  const onHandelIncrease = (id)=>{
setSingleImg(id);
  }

  const download = (e) => {
    console.log(e.target.href);
    e.preventDefault();
    fetch(e.target.href, {
      method: "GET",
      headers: {},
    })
      .then((response) => {
        response.arrayBuffer().then(function (buffer) {
          const url = window.URL.createObjectURL(new Blob([buffer]));
          const link = document.createElement("a");
          link.href = url;
          link.setAttribute("download", "image.jpeg");
          document.body.appendChild(link);
          link.click();
        });
      })
      .catch((err) => {
        console.log(err);
      });
  };

  return (
    <>
      {isshow && (
       
          <Container>
            <Close>
              <BsXLg
                className="btn"
                onClick={() => setShow(!isshow)}
                color="white"
                fontSize="1.3em"
              />
            </Close>
            <div className="arrow">
              <BsChevronLeft
                color="white"
                fontSize="2em"
                onClick={() => {
                  onHandelIncrease(Number(singledata.id) - 1);
                }}
              />
            </div>

            <Content>
              <Header>
                <UserInfo>
                  <img src="/images/user.svg" alt="" />
                  <span>{singledata.author}<p>@avaiable for hire</p></span>

                </UserInfo>
                <button>
                  <BsFillSuitHeartFill color="black" fontSize="1.5em" />
                </button>
                <button>
                  <img src="/images/plus-icon.svg" alt="close" />
                </button>
                <Dowload
                  href={singledata.download_url}
                  target="_blank"
                  onClick={(e) => download(e)}
                  download
                >
                  Download
                </Dowload>
                {/* <link  href={singledata.download_url} download>download</link> */}
              </Header>
              <Main>
                <img src={singledata.download_url} alt="images" />
              </Main>

              <Footer>
                <p>view</p>
                <p>Downloads</p>
                <p>Share</p>
                <p>More</p>
              </Footer>
            </Content>
            <div className="arrow">
              <BsChevronRight
                color="white"
                fontSize="2em"
                onClick={() => {
                  onHandelIncrease(Number(singledata.id) + 1);
                }}
              />
            </div>
          </Container>
      
      )}
    </>
  );
}


const Container = styled.div`
  position: fixed;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  z-index: 9999;
  color: black;
  background-color: rgba(0, 0, 0, 0.5);
  display: flex;
  
  .arrow {
    display: flex;
    justify-content: center;
    align-items: center;
    padding: 10px;
   
    @media (max-width: 740px) {
      /* flex-direction: column; */
      display: none;
    }
  }
  .btncls {
    margin-top: 20px;
    margin-left: 20px;
    padding-left: 10px;
  }
  @media (max-width: 740px) {
    flex-direction: column;
  }
`;

const Content = styled.div`
  width: 90%;

  background-color: white;
  border-radius: 5px;
  position: relative;
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  top: 10px;
  margin: 0 auto;
  overflow: scroll;
  overflow-x: hidden;
  &::-webkit-scrollbar {
    width: 0;
    height: 0;
  }

  @media (max-width: 740px) {
    margin-top: 20px;
  }
`;

const Header = styled.div`
  text-align: start;
  padding: 16px 20px;
  font-size: 16px;
  line-height: 1.5;
  color: rgba(0, 0, 0, 0.6);
  font-weight: 100;
  display: flex;
  justify-content: space-between;
  align-items: center;

  button {
    margin: 10px;
    height: 40px;
    width: 40px;
    min-height: auto;
    color: rgba(0, 0, 0, 0.15);
    img {
      pointer-events: none;
    }
    @media (max-width: 420px) {
      display: none;
    }
  }
`;
const Dowload = styled.a`
  background-color: #88cf42;
  padding: 10px 20px;
  color: white;
  border-radius: 5px;
  text-decoration: none;
  @media (max-width: 740px) {
    padding: 5px 10px;
  }
`;

const UserInfo = styled.div`
  flex: 1;
  display: flex;
  align-items: center;

  img {
    width: 48px;
    height: 48px;
    background-clip: content-box;
    border: 2px solid transparent;
    border-radius: 75%;
  }

  span {
    font-weight: 500;
    font-size: 16px;
    line-height: 1.5;
    margin-left: 5px;
    display: flex;
    flex-direction: column;
    p {
      color: #007fff;
      font-size: 11px;
    }
  }
  @media (max-width: 740px) {
    padding: 5px ;
  }
`;

const Main = styled.div`
  max-width: 900px;
  margin: 0 auto;
  img {
    width: 100%;
    @media (max-width: 740px) {
      height: 100%;
    }
  }
  @media (max-width: 740px) {
    height: 100vh;
  }
`;

const Footer = styled.div`
  text-align: start;
  padding: 16px 20px;
  font-size: 16px;
  line-height: 1.5;
  color: rgba(0, 0, 0, 0.6);
  font-weight: 100;
  display: flex;
  justify-content: space-between;
  align-items: center;
`;




const Close = styled.div`
  position: fixed;
  top: 0;
  left: 0;
  padding-top: 10px;
  padding-left: 10px;
  font-size: 10px;
  
`;






