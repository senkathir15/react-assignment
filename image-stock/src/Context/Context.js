import React from "react";
import { useState, useContext, useEffect, createContext } from "react";
const API = `https://picsum.photos/v2/list`;

export const Context = createContext();

export function Provider(prop) {
  const [listImage, setListImage] = useState([]);
  const [query, setQuery] = useState(1);
  const [isloading, setIsloading] = useState(true);
  const [isshow, setShow] = useState(false);
  const [singleImg, setSingleImg] = useState(0);
  const [singledata, setsingleDate] = useState({});
  const [title, setTitle] = useState("");

  const SINGAL_API = `https://picsum.photos/id/${singleImg}/info`;

  const API_ENDPOINT = `${API}?page=${query}&limit=60`;
  const fetchData = async (API) => {
    try {
      setIsloading(true);
      const response = await fetch(API);
      const data = await response.json();
      setListImage(data);
      setIsloading(false);
      console.log(data);
    } catch (error) {
      console.log(error);

    }
  };

  useEffect(() => {
    fetchData(API_ENDPOINT);
  }, [API_ENDPOINT]);

  useEffect(() => {
    const fetchsingleData = async (API) => {
      try {
        const response = await fetch(API);
        const data = await response.json();
        setsingleDate(data);
        console.log(data+"fgdfgf");
      } catch (error) {
        console.log(error);
        setsingleDate({
          id: "50",
          author: "Andrew Ridley",
          url: "https://unsplash.com/photos/Kt5hRENuotI",
          download_url: "https://picsum.photos/id/1018/3914/2935",
        });
      }
    };

    fetchsingleData(SINGAL_API);
   
  }, [SINGAL_API]);

  const value = {
    listImage,
    setQuery,
    query,
    isloading,
    setShow,
    isshow,
    singledata,
    setSingleImg,
    title,
    setTitle,
  };

  return <Context.Provider value={value}>{prop.children}</Context.Provider>;
}
