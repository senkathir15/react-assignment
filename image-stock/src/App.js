import './App.css';
import Main from './Components/Main';
import Navigation from './Components/Navigation';
import Modal from './Components/Modal';
// import DMain from './Components/DuplicateMain';
import Headers from './Components/Headers';
function App() {
  return (
    <>
    <Modal/>
    <Navigation/>
    <Headers/>
    <Main/>
    {/* <DMain/> */}
    </>
  );
}

export default App;
